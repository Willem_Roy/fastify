import Fastify from 'fastify'
// import testRoute from './routes/test/test'
import willRoute from './routes/willem/index.js'

const fastify = Fastify({
  logger: false
})

fastify.register(willRoute)

fastify.get('/', async (request, reply) => {
  return { hello: 'world' }
})

fastify.get('/willem', async (request, reply) => {
    return "Salut Willem"
  }) 

/**
 * Run the server!
 */
const start = async () => {
  try {
    await fastify.listen({ port: 3000, host: '0.0.0.0' })
    console.log("Serveur start on port 3000")
  } catch (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  
}
start()