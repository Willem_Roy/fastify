async function willRoute (fastify, options) {
    fastify.get('/will', async (request, reply) => {
      return { hello: 'world', message: 'test' }
    })
  }
  
  //ESM
  export default willRoute;