"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const fastify_1 = __importDefault(require("fastify"));
// import testRoute from './routes/test/test'
const index_js_1 = __importDefault(require("./routes/willem/index.js"));
const fastify = (0, fastify_1.default)({
    logger: false
});
fastify.register(index_js_1.default);
fastify.get('/', (request, reply) => __awaiter(void 0, void 0, void 0, function* () {
    return { hello: 'world' };
}));
fastify.get('/willem', (request, reply) => __awaiter(void 0, void 0, void 0, function* () {
    return "Salut Willem";
}));
/**
 * Run the server!
 */
const start = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield fastify.listen({ port: 3000, host: '0.0.0.0' });
        console.log("Serveur start on port 3000");
    }
    catch (err) {
        fastify.log.error(err);
        process.exit(1);
    }
});
start();
//# sourceMappingURL=server.js.map